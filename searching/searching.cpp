#include<iostream>
#include"searching.h"
#include"../sorting/selectionSort.h"
using namespace std;
#define MAX 20


binarysearch::binarysearch(){
	for(int i=0;i<MAX;i++){
		elements[i]=-1;
	}
}
binarysearch::~binarysearch(){
}
void binarysearch::insert(int data){
	int index=0;
	while(index<MAX){
		if(elements[index]==-1){
			elements[index]=data;
			break;
		}
		index++;
	}
	if(index==MAX)	cout<<"ARRAY FULL"<<endl;
}
void binarysearch::display(){
	for(int i=0;i<MAX;i++){
		cout<<elements[i]<<endl;
	}
}
bool binarysearch::bsearch(int target){
    int min=0;
    int mid;
    int max=MAX-1;
    while(max>min){
        mid=(min+max)/2;
        if(elements[mid]==target){
            return true;
        }else if(elements[mid]<target){
            min=mid+1;
        }else{
            max=mid-1;
        }
    }
    return false;
}
int main(){
    binarysearch a;
    a.insert(7);
    a.insert(10);
    a.insert(30);
    
    selectionSort(a.elements, MAX);
    
    a.display();
    if(a.bsearch(7))	cout<<"Exists"<<endl;
   
}
